# Abstraction duplet to use.
# Platforms:
#   lin -- Linux-specific
#   win -- Windows-specific
#   osx -- OS X-specific
#   gen -- Generic
# Output Methods:
#   hw  -- Use internal PC speaker (hardware)
#   emu -- Use an emulated square-wave (emulated)
ifeq ($(PLATFORM),)
PLATFORM  = gen
endif
ifeq ($(OUTMETHOD),)
OUTMETHOD = emu
endif
DUPLET    = $(PLATFORM)-$(OUTMETHOD)
# Validate duplet
ifneq ($(PLATFORM),lin)
ifneq ($(PLATFORM),win)
ifneq ($(PLATFORM),osx)
ifneq ($(PLATFORM),gen)
$(error Unsupported duplet $(DUPLET))
endif
endif
endif
endif
ifneq ($(OUTMETHOD),hw)
ifneq ($(OUTMETHOD),emu)
$(error Unsupported duplet $(DUPLET))
endif
endif

OBJECTS   = $(DUPLET)-pcspkr.o main.o compiler.o
OUTPUT    = zerstoren
INCLUDE   = -I./
LIBS      = -lm
ifeq ($(DUPLET),gen-emu)
LIBS     += -lportaudio
endif
ifeq ($(DUPLET),lin-emu)
LIBS     += -lportaudio
endif
ifeq ($(DUPLET),win-emu)
LIBS     += -lportaudio
endif
ifeq ($(DUPLET),osx-emu)
LIBS     += -lportaudio
endif

CFLAGS    = -std=c89 -Wall -Werror -Wextra -O0 -g -pedantic $(INCLUDE)
ifeq ($(PLATFORM),gen)
CFLAGS   += -DPLATFORM=0
ifeq ($(OUTMETHOD),emu)
CFLAGS   += -DOUTMETHOD=0 -DDUPLET=0
endif
ifeq ($(OUTMETHOD),hw)
CFLAGS   += -DOUTMETHOD=16 -DDUPLET=16
endif
endif
ifeq ($(PLATFORM),lin)
CFLAGS   += -DPLATFORM=1
ifeq ($(OUTMETHOD),emu)
CFLAGS   += -DOUTMETHOD=0 -DDUPLET=1
endif
ifeq ($(OUTMETHOD),hw)
CFLAGS   += -DOUTMETHOD=16 -DDUPLET=17
endif
endif
ifeq ($(PLATFORM),win)
CFLAGS   += -DPLATFORM=2
ifeq ($(OUTMETHOD),emu)
CFLAGS   += -DOUTMETHOD=0 -DDUPLET=2
endif
ifeq ($(OUTMETHOD),hw)
CFLAGS   += -DOUTMETHOD=16 -DDUPLET=18
endif
endif
ifeq ($(PLATFORM),osx)
CFLAGS   += -DPLATFORM=3
ifeq ($(OUTMETHOD),emu)
CFLAGS   += -DOUTMETHOD=0 -DDUPLET=3
endif
ifeq ($(OUTMETHOD),hw)
CFLAGS   += -DOUTMETHOD=16 -DDUPLET=19
endif
endif
LDFLAGS   = $(LIBS) -g

.PHONY: all install clean

all: $(OUTPUT)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@
clean:
	rm -f $(OUTPUT) $(OBJECTS) *-*-pcspkr.o

