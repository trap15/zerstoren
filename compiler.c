/*
	zerstören - A music utility for internal PC Speaker
	MML compiler

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>

#include "compiler.h"

uint16_t targetver = MAX_VERSION;

void usage(char *app);

uint8_t noteconvtbl[7] = {
	NOTE_A,
	NOTE_B,
	NOTE_C,
	NOTE_D,
	NOTE_E,
	NOTE_F,
	NOTE_G
};

#define NOTE_EMIT \
	tu8[1] = fgetc(ifp); \
	debug_printf("Handling %c (%02X)\n", tu8[1], tu8[1]); \
	if(targetver == 0) \
		tu8[0] = noteconvtbl[ch] + oct[side] * 12; \
	else \
		tu8[0] = noteconvtbl[ch]; \
	if(tu8[1] == '-') tu8[0]--; \
	else if(tu8[1] == '+') tu8[0]++; \
	else fseek(ifp, -1, SEEK_CUR); \
	if(!read_u8(ifp, tu8 + 1)) { \
		tu8[1] = len[side]; \
	} \
	tu8[2] = fgetc(ifp); \
	debug_printf("Handling %c (%02X)\n", tu8[2], tu8[2]); \
	if(tu8[2] == '.') tu8[1] += tu8[1] * 2; \
	else fseek(ifp, -1, SEEK_CUR); \
	tu8[1] &= 0xFF; \
	stream_write_u8(tu8[1], stream[side], &(streamptr[side])); \
	stream_write_u8(tu8[0], stream[side], &(streamptr[side]));

void stream_write_u8(uint8_t v, uint8_t *stream, uint32_t *ptr)
{
	stream[(*ptr)++] = v;
}

void stream_write_u16(uint16_t v, uint8_t *stream, uint32_t *ptr)
{
	stream[(*ptr)++] = (v & 0xFF00) >> 8;
	stream[(*ptr)++] = (v & 0x00FF);
}

int string_eol(FILE* fp, int ch)
{
	if(ch == EOF)
		return 1;
	if((ch == '\r') || (ch == '\n')) {
		fseek(fp, -1, SEEK_CUR);
		return 1;
	}
	return 0;
}

void string_prelude(FILE* fp)
{
	int ch;
	ch = fgetc(fp);
	for(; ((ch == ' ') || (ch == '\t')) && (ch != '\n') && (ch != EOF);) {
		ch = fgetc(fp);
	}
	if(string_eol(fp, ch))
		return;
	fseek(fp, -1, SEEK_CUR);
}

int read_u8(FILE* fp, uint8_t *n)
{
	int ch;
	int hit = 0;
	*n = 0;
	string_prelude(fp);
	for(;;) {
		ch = fgetc(fp);
		if(string_eol(fp, ch))
			break;
		if((ch < '0') || (ch > '9')) {
			fseek(fp, -1, SEEK_CUR);
			break;
		}
		if((*n >= 25) && (ch > '5')) {
			*n = 255;
			continue;
		}
		*n *= 10;
		*n += ch - '0';
		hit = 1;
	}
	return hit;
}

int read_u16(FILE* fp, uint16_t *n)
{
	int ch;
	int hit = 0;
	*n = 0;
	string_prelude(fp);
	for(;;) {
		ch = fgetc(fp);
		if(string_eol(fp, ch))
			break;
		if((ch < '0') || (ch > '9')) {
			fseek(fp, -1, SEEK_CUR);
			break;
		}
		if((*n == 6553) && (ch > '5')) {
			*n = 65535;
			continue;
		}
		*n *= 10;
		*n += ch - '0';
		hit = 1;
	}
	return hit;
}

int read_string(FILE* fp, char *s)
{
	int ch;
	int hit = 0;
	*s = 0;
	string_prelude(fp);
	for(;;) {
		ch = fgetc(fp);
		if(string_eol(fp, ch))
			break;
		*s++ = ch;
		*s = 0;
		hit++;
	}
	return hit;
}

int compiler_core(FILE* ifp, FILE* ofp)
{
	uint16_t tu16[16];
	uint8_t tu8[16];
	int16_t ts16[16];
	int8_t ts8[16];
	uint16_t tempo = 0;
	uint8_t oct[2] = {0, 0};
	uint8_t len[2] = {1, 1};
	uint8_t stream[2][65536];
	uint32_t streamptr[2] = {0, 0};
	char title[256], artist[256], copyright[256];
	uint8_t titlelen = 0, artistlen = 0, copyrightlen = 0;
	int ch;
	int side = 0;
	(void)ts16;
	(void)ts8;
	for(;;) {
		ch = fgetc(ifp);
		if(streamptr[0] > 65535) {
			fprintf(stderr, "Stream 0 overflow.\n");
			return 1;
		}
		if(streamptr[1] > 65535) {
			fprintf(stderr, "Stream 1 overflow.\n");
			return 1;
		}
		debug_printf("Parsing %c (%d)\n", ch, ch);
		switch(ch) {
			case EOF:
				goto finished;
			case 't':
				if(!read_u16(ifp, &tempo)) {
					fprintf(stderr, "t needs a parameter\n");
					return 1;
				}
				stream_write_u8(COMMAND_TEMPO, stream[side], &(streamptr[side]));
				stream_write_u16(tempo, stream[side], &(streamptr[side]));
				break;
			case 'o':
				if(!read_u8(ifp, &(oct[side]))) {
					fprintf(stderr, "o needs a parameter\n");
					return 1;
				}
				if(oct[side] > MAX_OCTAVE) {
					fprintf(stderr, "Octave must be <= 10\n");
					return 1;
				}
#if MAX_VERSION >= 1
				if(targetver >= 1) {
					stream_write_u8(COMMAND_OCTAVE | side << 6, stream[side], &(streamptr[side]));
					stream_write_u8(oct[side], stream[side], &(streamptr[side]));
				}
#endif
				break;
			case '<':
				debug_printf("Octave %d --\n", oct[side]);
				if(oct[side] == 0) {
					fprintf(stderr, "Octave %d:%d must be >= 0\n", side, oct[side]);
					return 1;
				}
				oct[side]--;
#if MAX_VERSION >= 1
				if(targetver >= 1) {
					stream_write_u8(COMMAND_OCTAVE | side << 6, stream[side], &(streamptr[side]));
					stream_write_u8(oct[side], stream[side], &(streamptr[side]));
				}
#endif
				break;
			case '>':
				debug_printf("Octave %d ++\n", oct[side]);
				if(oct[side] >= MAX_OCTAVE) {
					fprintf(stderr, "Octave %d:%d must be <= 10\n", side, oct[side]);
					return 1;
				}
				oct[side]++;
#if MAX_VERSION >= 1
				if(targetver >= 1) {
					stream_write_u8(COMMAND_OCTAVE | side << 6, stream[side], &(streamptr[side]));
					stream_write_u8(oct[side], stream[side], &(streamptr[side]));
				}
#endif
				break;

			case '[':
				stream_write_u8(COMMAND_OPENLOOP | side << 6, stream[side], &(streamptr[side]));
				break;
			case ']':
				stream_write_u8(COMMAND_CLOSELOOP | side << 6, stream[side], &(streamptr[side]));
				if(!read_u8(ifp, tu8)) {
					tu8[0] = 2;
				}
				tu8[0]--;
				stream_write_u8(tu8[0], stream[side], &(streamptr[side]));
				break;

			case 'l':
				if(!read_u8(ifp, &(len[side]))) {
					fprintf(stderr, "l needs a parameter\n");
					return 1;
				}
				if(fgetc(ifp) == '.') len[side] += len[side] * 2;
				else fseek(ifp, -1, SEEK_CUR);
				break;

			case 'r':
				if(!read_u8(ifp, tu8)) {
					tu8[0] = len[side];
				}
				if(fgetc(ifp) == '.') tu8[0] += tu8[0] * 2;
				else fseek(ifp, -1, SEEK_CUR);
				tu8[0] &= 0xFF;
#if MAX_VERSION >= 1
				if(targetver >= 1)
					stream_write_u8(COMMAND_REST | side << 6, stream[side], &(streamptr[side]));
#endif
				stream_write_u8(tu8[0], stream[side], &(streamptr[side]));
				if(targetver == 0)
					stream_write_u8(132, stream[side], &(streamptr[side]));
				break;

			case 'a':
			case 'b':
			case 'c':
			case 'd':
			case 'e':
			case 'f':
			case 'g':
				ch -= 'a';
				NOTE_EMIT
				break;
			case '|':
				debug_printf("End: %d\n", side);
				stream_write_u8(COMMAND_END, stream[side], &(streamptr[side]));
				break;
			case '@':
				tu8[0] = 1;
				for(; (ch != ' ') && (ch != '\t') && (ch != '\n') && (ch != EOF);) {
					ch = fgetc(ifp);
					tu8[tu8[0]++] = ch;
					if(tu8[0] == 16)
						break;
				}
				if((ch == '\n') || (ch == EOF))
					break;
				if(strncmp((char*)tu8 + 1, "TITLE", 5) == 0) {
					titlelen = read_string(ifp, title);
				}else if(strncmp((char*)tu8 + 1, "ARTIST", 6) == 0) {
					artistlen = read_string(ifp, artist);
				}else if(strncmp((char*)tu8 + 1, "COPYRIGHT", 9) == 0) {
					copyrightlen = read_string(ifp, copyright);
				}else if(strncmp((char*)tu8 + 1, "VERSION", 7) == 0) {
					if(!read_u16(ifp, &targetver))
						targetver = MAX_VERSION;
				}else{
					fprintf(stderr, "@ needs TITLE, ARTIST, COPYRIGHT, or VERSION.\n");
					return 1;
				}
				break;
			case '#':
				for(; (ch != '\n') && (ch != EOF);) {
					ch = fgetc(ifp);
				}
				break;
			case 'A':
				side = 0;
				break;
			case 'B':
				side = 1;
				break;
			case '\n':
			case ' ':
			case '\t':
			case '\r':
				break;
			default:
				fprintf(stderr, "Bad symbol %c (%02X)\n", ch, ch);
				break;
		}
	}
finished:
	tu16[0] = tu16[1] = 0;
	tu16[1] = 16 + titlelen + artistlen + copyrightlen + 3;
	tu16[1] = htons(tu16[1]);
	fwrite(tu16, 4, 1, ofp); /* Header length */
	tu8[0] = titlelen + 1;
	tu8[1] = artistlen + 1;
	tu8[2] = copyrightlen + 1;
	tu8[3] = targetver;
	fwrite(tu8, 4, 1, ofp); /* Information lengths */
	tu16[1] = 0;
	fwrite(tu16, 4, 1, ofp); /* Chan A stream pointer */
	tu16[1] = htons(streamptr[0]);
	fwrite(tu16, 4, 1, ofp); /* Chan B stream pointer */

	tu8[3] = 0;
	fwrite(title, titlelen, 1, ofp);
	fwrite(tu8 + 3, 1, 1, ofp);
	fwrite(artist, artistlen, 1, ofp);
	fwrite(tu8 + 3, 1, 1, ofp);
	fwrite(copyright, copyrightlen, 1, ofp);
	fwrite(tu8 + 3, 1, 1, ofp);

	fwrite(stream[0], streamptr[0], 1, ofp);
	fwrite(stream[1], streamptr[1], 1, ofp);
	return 0;
}

int zerstoren_compiler(int argc, char *argv[])
{
	FILE* ifp;
	FILE* ofp;
	if(argc < 4) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	if((ifp = fopen(argv[2], "rb")) == NULL) {
		perror("Unable to open source file");
		return EXIT_FAILURE;
	}
	if((ofp = fopen(argv[3], "wb+")) == NULL) {
		perror("Unable to open target file");
		return EXIT_FAILURE;
	}

	if(compiler_core(ifp, ofp)) {
		perror("Unable to convert MML");
		return EXIT_FAILURE;
	}

	fclose(ofp);
	fclose(ifp);

	return EXIT_SUCCESS;
}


