/*
	zerstören - A music utility for internal PC Speaker
	MML compiler

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef COMPILER_H
#define COMPILER_H

#define MAX_VERSION		(1)

#define COMMAND_TEMPO		0x80
#define COMMAND_OPENLOOP	0x81
#define COMMAND_CLOSELOOP	0x82
#define COMMAND_END		0x83
#if MAX_VERSION >= 1
#  define COMMAND_OCTAVE	0x84
#  define COMMAND_REST		0x85
#endif
#define COMMAND_MAX		0xBF /* 0xC0+ is reserved for side mess */

#define NOTE_C		0
#define NOTE_D		2
#define NOTE_E		4
#define NOTE_F		5
#define NOTE_G		7
#define NOTE_A		9
#define NOTE_B		11

#define MAX_OCTAVE	10

#define DEBUG		0

extern uint16_t targetver;

int zerstoren_compiler(int argc, char *argv[]);

#if DEBUG
#define debug_printf		printf
#else
static void debug_printf(char *x, ...) { (void)x; }
#endif

#endif

