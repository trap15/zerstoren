/*
	zerstören - A music utility for internal PC Speaker
	PC speaker hardware abstraction

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#define __USE_XOPEN_EXTENDED
#include <unistd.h>
#define __USE_POSIX199309
#include <time.h>
#include <arpa/inet.h>
#include "portaudio.h"

#include "pcspkr.h"

#define SAMPLERATE	(96000)
#define GENERATEMULT	(2)
#define SAMPLEFRAMEREQ	(1) /* paFramesPerBufferUnspecified could be nice */
#define VOLUME		(0.5)
#define RECORDRAW	(1)

typedef struct {
	uint16_t	freq;
	int64_t		len;
	uint32_t	phase;
	int		flipflop; /* 0 is high, 1 is low */
	int		loading; /* Waiting for input */
} pcspkr_data_t;

static pcspkr_data_t spkr_data;
static PaStream *stream;
static int spkr_playing = 0;
#if RECORDRAW
static FILE* rawfp = NULL;
#endif
static volatile int data_over = 0;
char* wavout = NULL;

#define INTERPOLATE_LENGTH	(17)
float interpsamples[INTERPOLATE_LENGTH];
double interpweights[INTERPOLATE_LENGTH] = {
	 0.007196076272467048,
	-0.002695549524364885,
	-0.024500390807801059,
	-0.046178156931166905,
	-0.036074093627460800,
	 0.030518699876579427,
	 0.143305402269119340,
	 0.252303266986499080,
	 0.297554515657714180,
	 0.252303266986499080,
	 0.143305402269119340,
	 0.030518699876579427,
	-0.036074093627460800,
	-0.046178156931166905,
	-0.024500390807801059,
	-0.002695549524364885,
	 0.007196076272467048,
};

static int pcspkr_generate_cb(const void *input, void *output,
			unsigned long frames,
			const PaStreamCallbackTimeInfo* tinf,
			PaStreamCallbackFlags sflags, void *user_data)
{
	pcspkr_data_t *data = user_data;
	signed long left = frames;
	float* out = output;
	uint32_t switchtime;
	float vol;
	double sample;
	int i, l;
#if RECORDRAW
	int8_t* orec;
	int8_t* rec;
	if(wavout) {
		orec = malloc(frames);
		rec = orec;
	}
#endif
	(void)input;
	(void)tinf;
	(void)sflags;
	if(data->freq == 0) /* We should probably just bail... */
		switchtime = 0xFFFFFFFF;
	else
		switchtime = (SAMPLERATE*GENERATEMULT) / data->freq;
	for(l = 0; l < left*GENERATEMULT; l++) {
		if(data->loading) {
			for(i = 0; i < INTERPOLATE_LENGTH - 1; i++)
				interpsamples[i] = interpsamples[i + 1];
			interpsamples[i] = 0;
			if((l % GENERATEMULT) == 0) {
				sample = 0;
				for(i = 0; i < INTERPOLATE_LENGTH; i++)
					sample += interpsamples[i] * interpweights[i];
#if RECORDRAW
				if(wavout)
					*rec++ = sample * 0x80;
#endif
				*out++ = sample;
			}
			continue;
		}
		else if(data->len <= 0) {
			vol = 0;
			data_over = 1;
		}else{
			vol = VOLUME;
			vol -= VOLUME / (data->phase + 2);
		}
		for(i = 0; i < INTERPOLATE_LENGTH - 1; i++)
			interpsamples[i] = interpsamples[i + 1];
		if(data->flipflop) {
			interpsamples[i] = -vol;
		}else{
			interpsamples[i] =  vol;
		}
		if((l % GENERATEMULT) == 0) {
			sample = 0;
			for(i = 0; i < INTERPOLATE_LENGTH; i++)
				sample += interpsamples[i] * interpweights[i];
#if RECORDRAW
			if(wavout)
				*rec++ = sample * 0x80;
#endif
			*out++ = sample;
			if(data->len > 0) {
				data->len--;
			}
		}
		if(data->len > 0) {
			data->phase++;
			if(data->phase >= switchtime) {
				data->flipflop ^= 1;
				data->phase -= switchtime;
			}
		}
	}
#if RECORDRAW
	if(wavout) {
		fwrite(orec, frames, 1, rawfp);
		free(orec);
	}
#endif
	return paContinue;
}

#define le32(t) (((t & 0xFF000000) >> 24) | \
		 ((t & 0x00FF0000) >>  8) | \
		 ((t & 0x0000FF00) <<  8) | \
		 ((t & 0x000000FF) << 24))

int pcspkr_init(void)
{
#if RECORDRAW
	char iff[4];
	uint32_t t32;
#endif
	PaStreamParameters params = {
		/* Device (Not set) */ 0,
		/* Channel Count */ 1,
		/* Sample Format */ paFloat32,
		/* Suggested latency */ 0.05,
		NULL };
	int i;
	const PaDeviceInfo* info;
	PaError err;
	err = Pa_Initialize();
	if(err != paNoError) {
		fprintf(stderr, "PortAudio error: %s\n", Pa_GetErrorText(err));
		return 1;
	}
	spkr_data.freq = 0;
	spkr_data.len = 0;
	spkr_data.phase = 0;
	spkr_data.flipflop = 0;
	spkr_data.loading = 1;
	data_over = 0;
	for(i = 0; i < INTERPOLATE_LENGTH; i++)
		interpsamples[i] = 0;
#if RECORDRAW
	/* | BLESS THIS MESS */
	/* V                 */

	if(wavout) {
		rawfp = fopen(wavout, "wb+");
		if(rawfp == NULL) {
			return 1;
		}
		/* RIFF header */
		iff[0] = 'R'; iff[1] = 'I'; iff[2] = 'F'; iff[3] = 'F';
		fwrite(iff, 4, 1, rawfp);
		t32 = 0xFFFFFFFF; /* Max it until we get back */
		t32 = le32(htonl(t32));
		fwrite(&t32, 4, 1, rawfp);
		iff[0] = 'W'; iff[1] = 'A'; iff[2] = 'V'; iff[3] = 'E';
		fwrite(iff, 4, 1, rawfp);
		/* "fmt " chunk */
		iff[0] = 'f'; iff[1] = 'm'; iff[2] = 't'; iff[3] = ' ';
		fwrite(iff, 4, 1, rawfp);
		iff[0] = 0x10; iff[1] = 0; iff[2] = 0; iff[3] = 0;
		fwrite(iff, 4, 1, rawfp);
		iff[0] = 1; iff[1] = 0; iff[2] = 1; iff[3] = 0;
		fwrite(iff, 4, 1, rawfp);
		t32 = SAMPLERATE;
		t32 = le32(htonl(t32));
		fwrite(&t32, 4, 1, rawfp);
		fwrite(&t32, 4, 1, rawfp);
		iff[0] = 1; iff[1] = 0; iff[2] = 8; iff[3] = 0;
		fwrite(iff, 4, 1, rawfp);
		/* "data" chunk */
		iff[0] = 'd'; iff[1] = 'a'; iff[2] = 't'; iff[3] = 'a';
		fwrite(iff, 4, 1, rawfp);
		t32 = 0xFFFFFFFF - 36; /* Max it until we get back */
		t32 = le32(htonl(t32));
		fwrite(&t32, 4, 1, rawfp);
	}
#endif
#if 0
	err = Pa_OpenDefaultStream(
				&stream, 0, 1, /* 0 chan in, 1 chan out */
				paFloat32, SAMPLERATE, SAMPLEFRAMEREQ,
				pcspkr_generate_cb, &spkr_data);
#else
	for(i = Pa_GetDeviceCount(); i >= 0; i--) {
		info = Pa_GetDeviceInfo(i);
		if(!info) continue;
		params.device = i;
		err = Pa_OpenStream(	&stream, NULL, &params, SAMPLERATE,
					SAMPLEFRAMEREQ, 0, pcspkr_generate_cb,
					&spkr_data);
		if(err == paNoError) {
			return 0;
		}
	}
#endif
	fprintf(stderr, "PortAudio error: No working device found.\n");
	return 1;
}

int pcspkr_tone(uint16_t freq, uint16_t len)
{
	PaError err;
	pcspkr_data_t* data;
	data = &spkr_data;
	spkr_data.loading = 1;
	spkr_data.phase = 0;
	spkr_data.flipflop = 0;
	spkr_data.freq = freq;
	spkr_data.len = len * (SAMPLERATE / 1000);
	data_over = 0;
	if(!spkr_playing) {
		err = Pa_StartStream(stream);
		if(err != paNoError) {
			fprintf(stderr, "PortAudio error: %s\n", Pa_GetErrorText(err));
			return 3;
		}
		spkr_playing = 1;
	}
	spkr_data.loading = 0;
	while(!data_over) usleep(50);
	return 0;
}

int pcspkr_fini(void)
{
	PaError err;
#if RECORDRAW
	uint32_t size;
	uint32_t t32;
#endif
	if(spkr_playing) {
		err = Pa_StopStream(stream);
		if(err != paNoError) {
			fprintf(stderr, "PortAudio error: %s\n", Pa_GetErrorText(err));
			return 2;
		}
		spkr_playing = 0;
	}
#if RECORDRAW
	if(wavout) {
		/* Finishing touches */
		size = ftell(rawfp);
		fseek(rawfp, 4, SEEK_SET);
		t32 = size - 8;
		t32 = le32(htonl(t32));
		fwrite(&t32, 4, 1, rawfp);
		fseek(rawfp, 40, SEEK_SET);
		t32 = size - 44;
		t32 = le32(htonl(t32));
		fwrite(&t32, 4, 1, rawfp);
		fclose(rawfp);
	}
#endif
	err = Pa_Terminate();
	if(err != paNoError) {
		fprintf(stderr, "PortAudio error: %s\n", Pa_GetErrorText(err));
		return 2;
	}
	return 0;
}

