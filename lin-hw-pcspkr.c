/*
	zerstören - A music utility for internal PC Speaker
	PC speaker hardware abstraction

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <linux/kd.h>
#include <asm/param.h>
#include <sys/ioctl.h>
#include <unistd.h>
#define __USE_POSIX199309
#include <time.h>

#include "pcspkr.h"

static int _pcspkr_fd = -1;

int pcspkr_init(void)
{
	_pcspkr_fd = open("/dev/tty10", O_RDONLY);
	if(_pcspkr_fd == -1)
		return 1;
	return 0;
}

int pcspkr_tone(uint16_t freq, uint16_t len)
{
	struct timespec ts;
	uint16_t olen;
	olen = (float)len * (float)HZ / 100.0f;
	if(freq != 0)
		freq = 1193180 / freq;
	if(ioctl(_pcspkr_fd, KDMKTONE, (olen << 16) | freq) == -1)
		return 3;
	ts.tv_sec = 0;
	ts.tv_nsec = (long)len * 1000000;
	ts.tv_nsec -= SPKR_FUDGE;
	nanosleep(&ts, NULL);
	return 0;
}

int pcspkr_fini(void)
{
	if(close(_pcspkr_fd) == -1)
		return 2;
	return 0;
}

