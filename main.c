/*
	zerstören - A music utility for internal PC Speaker
	Main code

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <arpa/inet.h>

#include "pcspkr.h"
#include "compiler.h"

#define NOTEGEN_ROOT	1.05946309436

#define PLAY_PART	(ARPEGGIOS * 4)
#define TIME_FUDGE	(5)

static int tempo, whole;
static uint8_t l_note = 0, r_note = 0; /* Not really stereo, just a visual concept. */
static uint8_t l_octave = 0, r_octave = 0;
static int32_t l_len = 0, r_len = 0;
static float note_freqs[12 * MAX_OCTAVE];
static char note_names[256][256];

static uint8_t *mml;
static size_t mmlsize;
static size_t l_ptr, r_ptr;
static size_t l_loops[16][2], r_loops[16][2]; /* loops[*][0] is loop position, [1] is count left */
static int l_stackpos = 16, r_stackpos = 16;

int note_generate(void)
{
	int i;
	const char* nname[] = {
		"C ", "C#", "D ", "D#", "E ",
		"F ", "F#", "G ", "G#", "A ", "A#", "B " };
	for(i = 0; i < (12 * MAX_OCTAVE); i++) {
		note_freqs[i] = 16.35 * pow(NOTEGEN_ROOT, i);
		note_names[i][0] = nname[i % 12][0];
		note_names[i][1] = nname[i % 12][1];
		note_names[i][2] = (i / 12) + '0';
		note_names[i][3] = 0;
	}
	for(; i < 256; i++) {
		note_names[i][0] = note_names[i][1] = note_names[i][2] = ' ';
		note_names[i][3] = 0;
	}
	return 0;
}

#define NOTE_PLAY(F, T) do {\
	if((ret = pcspkr_tone(F, T))) { \
		fprintf(stderr, "Unable to play PC speaker note: %d (%s)\n", ret, \
					get_error_code(ret)); \
		perror("Reason"); \
		exit(EXIT_FAILURE); \
	} \
} while(0)

#define KNOWN_ERRORS	(6)

char* errorcodes[KNOWN_ERRORS + 1] = {
	"Unknown",
	"None",
	"Unable to open speaker",
	"Unable to close speaker",
	"Unable to make tone",
	"Unable to open MML file",
	"Unable to allocate MML",
};

char* get_error_code(int code)
{
	if(code >= KNOWN_ERRORS)
		return errorcodes[0];
	return errorcodes[code + 1];
}

int get_mml_length(char len)
{
	int i;
	int ret = 0;
	debug_printf("len1: %02X\n", len);
	for(i = 0; i < 6; i++) {
		ret /= 2;
		if(len & 0x20)
			ret += whole;
		len <<= 1;
	}
	debug_printf("len2: %d\n", ret);
	return ret;
}

uint16_t play_note_part(uint8_t n, uint16_t nl)
{
	int ret;
	uint16_t low = PLAY_PART;
	if(nl < low)
		low = nl;
	if(n < (12 * (MAX_OCTAVE + 1)))
		NOTE_PLAY(note_freqs[n], low);
	else
		NOTE_PLAY(0, low);
	return low;
}

uint16_t play_arpeg_part(uint8_t l, uint8_t r, uint16_t ll, uint16_t rl)
{
	int ret;
	int i;
	int len;
	uint16_t low = PLAY_PART;
	if(ll < low)
		low = ll;
	if(rl < low)
		low = rl;
	if(l >= (12 * (MAX_OCTAVE + 1))) {
		play_note_part(r, low);
		return low;
	}
	if(r >= (12 * (MAX_OCTAVE + 1))) {
		play_note_part(l, low);
		return low;
	}
	for(i = low; i > 0; ) {
		if(i < ARPEGGIOS)
			len = i;
		else
			len = ARPEGGIOS;
		if(l < (12 * (MAX_OCTAVE + 1)))
			NOTE_PLAY(note_freqs[l], len);
		else
			NOTE_PLAY(0, len);
		i -= len;
		if(i < ARPEGGIOS)
			len = i;
		else
			len = ARPEGGIOS;
		if(r < (12 * (MAX_OCTAVE + 1)))
			NOTE_PLAY(note_freqs[r], len);
		else
			NOTE_PLAY(0, len);
		i -= len;
	}
	return low;
}

int conv_tempo_whole(void)
{
	return 240000.0f / (float)tempo;
}

size_t mml_control(uint8_t c, uint8_t *mml, size_t i)
{
	switch(c & 0xBF) {
		case COMMAND_TEMPO:
			tempo  = (mml[++i] << 8);
			tempo |=  mml[++i];
			whole  = conv_tempo_whole();
			break;
		case COMMAND_OPENLOOP:
			if(c & 0x40) {
				r_stackpos--;
				r_loops[r_stackpos][0] = i;
				r_loops[r_stackpos][1] = 0;
				debug_printf("Rloopback %08X\n", (int)r_loops[r_stackpos][0]);
			}else{
				l_stackpos--;
				l_loops[l_stackpos][0] = i;
				l_loops[l_stackpos][1] = 0;
				debug_printf("Lloopback %08X\n", (int)l_loops[l_stackpos][0]);
			}
			break;
		case COMMAND_CLOSELOOP:
			if(c & 0x40) {
				if(r_loops[r_stackpos][1] == 0)
					r_loops[r_stackpos][1] = mml[++i] | 0x80;
				if(r_loops[r_stackpos][1] == 0x80) {
					r_stackpos++;
					i++;
					break;
				}
				r_loops[r_stackpos][1]--;
				debug_printf("RLooping back to %08X %d\n", (int)r_loops[r_stackpos][0], 
							(int)r_loops[r_stackpos][1]);
				return r_loops[r_stackpos][0];
			}else{
				if(l_loops[l_stackpos][1] == 0)
					l_loops[l_stackpos][1] = mml[++i] | 0x80;
				if(l_loops[l_stackpos][1] == 0x80) {
					l_stackpos++;
					i++;
					break;
				}
				l_loops[l_stackpos][1]--;
				debug_printf("LLooping back to %08X %d\n", (int)l_loops[l_stackpos][0], 
							(int)l_loops[l_stackpos][1]);
				return l_loops[l_stackpos][0];
			}
			break;
#if MAX_VERSION >= 1
		case COMMAND_OCTAVE:
			if(c & 0x40) {
				r_octave = mml[++i];
			}else{
				l_octave = mml[++i];
			}
			break;
		case COMMAND_REST:
			if(c & 0x40) {
				r_len = get_mml_length(mml[++i] & 0x7F);
				r_note = 255;
			}else{
				l_len = get_mml_length(mml[++i] & 0x7F);
				l_note = 255;
			}
			break;
#endif
		case COMMAND_END:
			return 0;
	}
	return i;
}

#define FETCHCORE(side) { \
	int8_t ts8; \
	if(side##_ptr < 8) { \
/*		debug_printf("Skip " #side " fetch\n");*/ \
		return; \
	} \
	debug_printf(#side "Fetched: %02X\n", mml[side##_ptr]); \
	if(mml[side##_ptr] & 0x80) { \
		ts8 = side##_note; \
		side##_ptr = mml_control(mml[side##_ptr], mml, side##_ptr); \
		side##_ptr++; \
		if((ts8 == side##_note) || (side##_note != 255)) \
			side##_fetch(); \
	}else{ \
		side##_len = get_mml_length(mml[side##_ptr++] & 0x7F); \
		side##_note = mml[side##_ptr++]; \
		if(targetver >= 1) { \
			ts8 = (side##_note ^ 0x80) - 0x80; \
			side##_note = ts8 + (side##_octave * 12); \
		} \
	} \
}

void l_fetch(void)
{
	FETCHCORE(l)
}

void r_fetch(void)
{
	FETCHCORE(r)
}

void run_notes(void)
{
	uint16_t played;
	if(l_len <= TIME_FUDGE) {
		l_fetch();
	}
	if(r_len <= TIME_FUDGE) {
		r_fetch();
	}
	if((l_len > TIME_FUDGE) && (r_len > TIME_FUDGE)) {
		debug_printf("arpeg  %d %d %d %d\n", l_note, r_note,
				l_len, r_len);
		played = play_arpeg_part(l_note, r_note, l_len, r_len);
		l_len -= played;
		r_len -= played;
		debug_printf("arpeg2 %d %d %d %d\n", l_note, r_note,
				l_len, r_len);
	}else if(l_len > TIME_FUDGE) {
		debug_printf("left   %d %d\n", l_note, l_len);
		played = play_note_part(l_note, l_len);
		l_len -= played;
		debug_printf("left2  %d %d\n", l_note, l_len);
	}else if(r_len > TIME_FUDGE) {
		debug_printf("right  %d %d\n", r_note, r_len);
		played = play_note_part(r_note, r_len);
		r_len -= played;
		debug_printf("right2 %d %d\n", r_note, r_len);
	}
	if(l_len <= TIME_FUDGE) {
		if(r_len > TIME_FUDGE)
			r_len -= l_len;
	}
	if(r_len <= TIME_FUDGE) {
		if(l_len > TIME_FUDGE)
			l_len -= r_len;
	}
}

int run_mml(char* file)
{
	char *title;
	char *artist;
	char *copyright;
	FILE* fp;
	if((fp = fopen(file, "rb")) == NULL)
		return 4;
	fseek(fp, 0, SEEK_END);
	mmlsize = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	mml = malloc(mmlsize);
	if(mml == NULL)
		return 5;
	fread(mml, mmlsize, 1, fp);
	fclose(fp);
	title = malloc(mml[4]);
	artist = malloc(mml[5]);
	copyright = malloc(mml[6]);
	targetver = mml[7];
	strncpy(title, (char*)mml + 0x10, mml[4]);
	strncpy(artist, (char*)mml + 0x10 + mml[4], mml[5]);
	strncpy(copyright, (char*)mml + 0x10 + mml[4] + mml[5], mml[6]);
	if(mml[4] > 1)
		printf("Title    : %s\n", title);
	if(mml[5] > 1)
		printf("Artist   : %s\n", artist);
	if(mml[6] > 1)
		printf("Copyright: %s\n", copyright);
	printf("Zerstören data version %d\n", targetver);
	l_ptr = ntohl(((uint32_t*)mml)[2]) + ntohl(((uint32_t*)mml)[0]);
	r_ptr = ntohl(((uint32_t*)mml)[3]) + ntohl(((uint32_t*)mml)[0]);
	debug_printf("Left  ptr: %08X\n", (uint32_t)l_ptr);
	debug_printf("Right ptr: %08X\n", (uint32_t)r_ptr);
	fprintf(stderr, "\n");
	for(; (l_ptr >= 8) || (r_ptr >= 8);) {
#if !DEBUG
		fprintf(stderr, "\x1b[2K\x1b[0G => Playing: %s %4d         %s %4d",
				note_names[l_note],
				l_note != 132 ? l_len : 0,
				note_names[r_note],
				r_note != 132 ? r_len : 0);
#endif
		run_notes();
	}
	fprintf(stderr, "\n");
	free(mml);
	return 0;
}

void usage(char *app)
{
	fprintf(stderr,
		"Invalid arguments. Usage:\n"
#if OUTMETHOD == 0
		"	%s p song.czs [output.wav]\n"
#else
		"	%s p song.czs\n"
#endif
		"	    or\n"
		"	%s c in.zsm out.czs\n",
		app, app);
}

int zerstoren_player(int argc, char *argv[])
{
	int ret;
	if(argc < 3) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
#if OUTMETHOD == 0
	if(argc > 3)
		wavout = argv[3];
#endif

	if((ret = pcspkr_init())) {
		fprintf(stderr, "Unable to init PC speaker: %d (%s)\n", ret, 
					get_error_code(ret));
		perror("Reason");
		return EXIT_FAILURE;
	}

	if((ret = note_generate())) {
		fprintf(stderr, "Unable to generate note table: %d (%s)\n", ret, 
					get_error_code(ret));
		perror("Reason");
		return EXIT_FAILURE;
	}

	if((ret = run_mml(argv[2]))) {
		fprintf(stderr, "Unable to play MML: %d (%s)\n", ret, 
					get_error_code(ret));
		perror("Reason");
		return EXIT_FAILURE;
	}

	if((ret = pcspkr_fini())) {
		fprintf(stderr, "Unable to close PC speaker: %d (%s)\n", ret, 
					get_error_code(ret));
		perror("Reason");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
	fprintf(stderr,
		"Zerstören, a music utility for internal PC speaker, "
			"by Alex Marshall\n");
	if(argc < 2) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	switch(argv[1][0]) {
		case 'p':
		case 'P':
			return zerstoren_player(argc, argv);
		case 'c':
		case 'C':
			return zerstoren_compiler(argc, argv);
	}
	return EXIT_FAILURE;
}

