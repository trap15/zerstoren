/*
	zerstören - A music utility for internal PC Speaker
	PC speaker hardware abstraction

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef PCSPKR_H
#define PCSPKR_H

#include <stdint.h>

#if DUPLET == 0
/* gen-emu */
#define ARPEGGIOS	(20)
#elif DUPLET == 1
/* lin-emu */
#define ARPEGGIOS	(20)
#elif DUPLET == 2
/* win-emu */
#define ARPEGGIOS	(20)
#elif DUPLET == 3
/* osx-emu */
#define ARPEGGIOS	(20)
#elif DUPLET == 17
/* lin-hw */
#define ARPEGGIOS	(22)
#define SPKR_FUDGE	(250)
#else
#error "unknown duplet"
#endif

#if OUTMETHOD == 0
extern char* wavout;
#endif

int pcspkr_init(void);
int pcspkr_fini(void);
/* Freq is Hz, len is ms */
int pcspkr_tone(uint16_t freq, uint16_t len);

#endif

